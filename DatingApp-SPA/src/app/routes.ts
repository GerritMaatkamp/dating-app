import { Routes } from '@angular/router';
// routes and data
import { AuthGuard } from './_guards/auth.guard';
import { MemberDetailResolver } from './_resolvers/member-detail.resolver';
import { MemberListResolver } from './_resolvers/member-list.resolver';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { ListsResolver } from './_resolvers/lists.resolver';
// components
import { HomeComponent } from './home/home.component';
import { MemberListComponent } from "./members/member-list/member-list.component";
import { ListsComponent } from "./lists/lists.component";
import { MessagesComponent } from "./messages/messages.component";
import { MemberDetailComponent } from './members/member-detail/member-detail.component';
import { MembersEditComponent } from './members/members-edit/members-edit.component';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { MessagesResolver } from './_resolvers/messages.resolver';


export const appRoutes: Routes = [
         { path: "", component: HomeComponent },
         { path: "home", component: HomeComponent },
         {
           path: "",
           runGuardsAndResolvers: "always",
           canActivate: [AuthGuard],
           children: [
             {
               path: "members",
               component: MemberListComponent,
               resolve: { user: MemberListResolver }
             },
             {
               path: "members/:id",
               component: MemberDetailComponent,
               resolve: { user: MemberDetailResolver }
             },
             {
               path: "member/edit",
               component: MembersEditComponent,
               resolve: { user: MemberEditResolver },
               canDeactivate: [PreventUnsavedChanges]
             },
             { path: "messages", component: MessagesComponent, resolve: { messages: MessagesResolver } },
             { path: "lists", component: ListsComponent, resolve: {users: ListsResolver} }
           ]
         },
         { path: "**", redirectTo: "home", pathMatch: "full" }
       ];